
import pandas as pd
import dash_core_components as dcc
import dash_html_components as html

from dash.dependencies import Input, Output, State

from app import app
from styles import main_tab_sel, main_tab_unsel, sec_tab_sel, sec_tab_unsel, sidebar_style, main_style, info_style, header_style, header_style2
from contents import create_contents
from utils import read_data, create_correlogram, create_running_mean_graph, create_121_filter_graph, create_lanczos_filter_graph, \
create_spectral_analysis_graph, create_regime_shift_graph


@app.callback(
    Output('tabs', 'children'),
    [Input('tabs', 'value')],
    state=[State('tabs', 'children')])
def update_tabs(tab_val, cur_children):

    if tab_val == 'uni-tab':
        children=[
            dcc.Tab(
                id='univariate-tab',
                label='Univariate',
                value='uni-tab',
                style=main_tab_unsel,
                selected_style=main_tab_unsel
            ),
            dcc.Tab(
                id='univariate-tab-exploratory',
                label='Exploratory Analysis',
                value='uni-tab-exploratory',
                style=sec_tab_unsel,
                selected_style=sec_tab_sel
            ),
            dcc.Tab(
                id='univariate-tab-filters',
                label='Filters',
                value='uni-tab-filters',
                style=sec_tab_unsel,
                selected_style=sec_tab_sel
            ),
            dcc.Tab(
                id='univariate-tab-spectral',
                label='Spectral Analysis',
                value='uni-tab-spectral',
                style=sec_tab_unsel,
                selected_style=sec_tab_sel
            ),
            dcc.Tab(
                id='univariate-tab-regimeshift',
                label='Regime Shift',
                value='uni-tab-regimeshift',
                style=sec_tab_unsel,
                selected_style=sec_tab_sel
            ),
            dcc.Tab(
                id='multivariate-tab',
                label='Multivariate',
                value='multi-tab',
                style=main_tab_unsel,
                selected_style=main_tab_unsel
            ),
            dcc.Tab(
                id='reference-tab',
                label='References',
                value='ref-tab',
                style=main_tab_unsel
            )
        ]
    
    elif tab_val == 'multi-tab':
        children=[
            dcc.Tab(
                id='univariate-tab',
                label='Univariate',
                value='uni-tab',
                style=main_tab_unsel,
                selected_style=main_tab_unsel
            ),
            dcc.Tab(
                id='multivariate-tab',
                label='Multivariate',
                value='multi-tab',
                style=main_tab_unsel,
                selected_style=main_tab_unsel
            ),
            dcc.Tab(
                id='multivariate-tab-correl',
                label='Correlation Matrix',
                value='multi-tab-correl',
                style=sec_tab_unsel,
                selected_style=sec_tab_sel
            ),
            dcc.Tab(
                id='multivariate-tab-eof',
                label='Empirical Orthogonal Function',
                value='multi-tab-eof',
                style=sec_tab_unsel,
                selected_style=sec_tab_sel
            ),
            dcc.Tab(
                id='reference-tab',
                label='References',
                value='ref-tab',
                style=main_tab_unsel,
                selected_style=main_tab_unsel
            )
        ]
    elif tab_val == 'ref-tab':
        children=[
            dcc.Tab(
                id='univariate-tab',
                label='Univariate',
                value='uni-tab',
                style=main_tab_unsel,
                selected_style=main_tab_unsel
            ),
            dcc.Tab(
                id='multivariate-tab',
                label='Multivariate',
                value='multi-tab',
                style=main_tab_unsel,
                selected_style=main_tab_unsel
            ),
            dcc.Tab(
                id='reference-tab',
                label='References',
                value='ref-tab',
                style=main_tab_unsel,
                selected_style=main_tab_unsel
            )
        ]

    else:
        children = cur_children

    return children


@app.callback(
    [Output('content', 'children'),
     Output('header', 'style'),
     Output('main', 'style'),
     Output('sidebar', 'children'),
     Output('sidebar', 'style'),
     Output('init-infos', 'style')],
    [Input('tabs', 'value')],
    state=[State('df-uni', 'data'),
           State('df-multi', 'data')]
)
def update_content(tab_val, data_uni, data_multi):

    content, sidebar = create_contents(tab_val, data_uni, data_multi)

    if tab_val == 'uni-tab' or tab_val == 'multi-tab' or tab_val == 'ref-tab' or tab_val == None or tab_val == 'tab-1':
        h_style = header_style
        c_style = {}
        sbar_style = {}
        if tab_val == 'tab-1':
            i_style = info_style
        else:
            i_style = {'display': 'none'}
    else:
        h_style = header_style2
        sbar_style = sidebar_style
        c_style = main_style
        i_style = {'display': 'none'}

    return content, h_style, c_style, sidebar, sbar_style, i_style


@app.callback(
    [Output('df-uni', 'data'),
     Output('output-msg-uni', 'message'),
     Output('output-msg-uni', 'displayed')],
    [Input('data-upload', 'filename'),
     Input('data-upload', 'contents')],
    state=[State('tabs', 'value')]
)
def update_data_uni(filename, file_content, tab_val):

    if tab_val == 'uni-tab' and filename != None:
        df, outmsg = read_data(filename, file_content)
        if df.shape[0] == 0:
            outdata = None
        else:
            outdata = df.to_json()
        disp = True
    else:
        outdata = None
        outmsg = None
        disp = False

    return outdata, outmsg, disp


@app.callback(
    [Output('df-multi', 'data'),
     Output('output-msg-multi', 'message'),
     Output('output-msg-multi', 'displayed')],
    [Input('data-upload', 'filename'),
     Input('data-upload', 'contents')],
    state=[State('tabs', 'value')]
)
def update_data_multi(filename, file_content, tab_val):

    if tab_val == 'multi-tab' and filename != None:
        df, outmsg = read_data(filename, file_content)
        if df.shape[0] == 0:
            outdata = None
        else:
            outdata = df.to_json()
        disp = True
    else:
        outdata = None
        outmsg = None
        disp = False

    return outdata, outmsg, disp


@app.callback(
    Output('correlogram', 'figure'),
    [Input('correlogram-lags', 'value'),
     Input('correlogram-sig', 'value')],
    state=[State('df-uni', 'data')]
)
def update_correlogram(lags, sig, data_uni):

    df = pd.read_json(data_uni, convert_dates=True)
    if lags != None and sig != None:
        return create_correlogram(df, lags, sig)
    else:
        return create_correlogram(df)


@app.callback(
    Output('running-mean', 'figure'),
    [Input('running-mean-window', 'value')],
    state=[State('df-uni', 'data')]
)
def update_running_mean_graph(window, data_uni):

    df = pd.read_json(data_uni, convert_dates=True)
    
    if window != None:
        return create_running_mean_graph(df, window)
    else:
        return create_running_mean_graph(df)


@app.callback(
    Output('121-filter', 'figure'),
    [Input('121-filter-n', 'value')],
    state=[State('df-uni', 'data')]
)
def update_121_filter_graph(n, data_uni):

    df = pd.read_json(data_uni, convert_dates=True)
    
    if n != None:
        return create_121_filter_graph(df, n)
    else:
        return create_121_filter_graph(df)


@app.callback(
    Output('lanczos-filter', 'figure'),
    [Input('lanczos-window', 'value'),
     Input('lanczos-cutoff1', 'value'),
     Input('lanczos-cutoff2', 'value')],
    state=[State('df-uni', 'data')]
)
def update_lanczos_filter_graph(window, cutoff1, cutoff2, data_uni):

    df = pd.read_json(data_uni, convert_dates=True)
    
    if window != None and cutoff1 != None and cutoff2 != None:
        if (window % 2) == 0:
            window += 1
        return create_lanczos_filter_graph(df, window, cutoff1, cutoff2)
    else:
        return create_lanczos_filter_graph(df)


@app.callback(
    Output('spectral-graph', 'figure'),
    [Input('spectral-tapper', 'value'),
     Input('spectral-window', 'value'),
     Input('spectral-sig', 'value'),
     Input('spectral-detrend', 'value')],
    state=[State('df-uni', 'data')]
)
def update_spectral_graph(tapper, window, sig, detrend, data_uni):

    df = pd.read_json(data_uni, convert_dates=True)
    if tapper != None and window != None and sig != None and detrend != None:
        return create_spectral_analysis_graph(df, tapper, window, sig, detrend)
    else:
        return create_spectral_analysis_graph(df)


@app.callback(
    Output('regime-graphs', 'figure'),
    [Input('regime-cutoff', 'value'),
     Input('regime-sig', 'value')],
    state=[State('df-uni', 'data')]
)
def update_regime_graph(cutoff, sig, data_uni):

    df = pd.read_json(data_uni, convert_dates=True)
    if cutoff != None and sig != None:
        return create_regime_shift_graph(df, cutoff, sig)
    else:
        return create_regime_shift_graph(df)