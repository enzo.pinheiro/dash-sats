
import pandas as pd
import dash_daq as daq
import dash_core_components as dcc
import dash_html_components as html

from styles import button_style, upload_style, main_tab_sel, main_tab_unsel, sec_tab_sel, sec_tab_unsel, ref_style
from infos import infos_ts_uni, infos_ts_multi, reference_info
from utils import create_timeseries, create_correlogram, create_boxplot, create_histogram, create_stat_table, create_running_mean_graph, \
create_121_filter_graph, create_lanczos_filter_graph, create_spectral_analysis_graph, create_regime_shift_graph, create_correl_heatmap, \
create_eof_graphs


def create_contents(tab_val, data_uni, data_multi):
    
    if tab_val == 'uni-tab':
        content = html.Div(
            children=[
                dcc.Markdown(infos_ts_uni),
                html.A("Open data example",
                    id='example-uni',
                    download='example_uni.csv',
                    href="https://gitlab.com/enzo.pinheiro/dash-sats/-/raw/master/uni_example.csv",
                    target="_blank"
                ),
                html.Hr(),
                dcc.Upload(
                        html.Button(
                            "Upload time series",
                            id='upload-button', 
                            style=button_style 
                        ), 
                    id='data-upload',
                )
            ],
            style=upload_style
        )

        sidebar = html.Div()

    elif tab_val == 'uni-tab-exploratory':
        if data_uni != None:
            df = pd.read_json(data_uni, convert_dates=True)
            content = html.Div(
                children=[
                    html.Div(className='row',
                        children=[
                            dcc.Graph(className='twelve columns',
                                id='time-series',
                                figure=create_timeseries(df)
                            )
                        ]
                    ),
                    html.Div(className='row',
                        children=[
                            dcc.Graph(className='six columns',
                                id='correlogram',
                                figure=create_correlogram(df)
                            ),
                            dcc.Graph(className='six columns',
                                id='boxplot',
                                figure=create_boxplot(df)
                            ),
                        ]
                    ),
                    html.Div(className='row',
                        children=[
                            dcc.Graph(className='six columns',
                                id='histogram',
                                figure=create_histogram(df)
                            ),
                            dcc.Graph(className='six columns',
                                id='table-skew',
                                figure=create_stat_table(df)
                            )
                        ]
                    )
                ]
            )

            sidebar = html.Div(
                children=[
                    html.H3("Settings"),
                    html.Hr(),
                    html.H5("Correlogram"),
                    html.P("Lags"),
                    dcc.Input(id='correlogram-lags', type="number", min=0, value=24),
                    html.P(),
                    html.P("Significance Level"),
                    dcc.Input(id='correlogram-sig', type="number", min=1, max=100, value=5),
                ]
            )

        else:
            content = html.Div()
            sidebar = html.Div()

    elif tab_val == 'uni-tab-filters':
        if data_uni != None:
            df= pd.read_json(data_uni, convert_dates=True)
            content = html.Div(
                children=[
                    html.Div(
                        dcc.Graph(className='twelve columns',
                        id='running-mean',
                        figure=create_running_mean_graph(df))
                    ),
                    html.Div(
                        dcc.Graph(className='twelve columns',
                        id='121-filter',
                        figure=create_121_filter_graph(df))
                    ),
                    html.Div(
                        dcc.Graph(className='twelve columns',
                        id='lanczos-filter',
                        figure=create_lanczos_filter_graph(df))
                    )
                ]
            ),
            sidebar = html.Div(
                children=[
                    html.H3("Settings"),
                    html.Hr(),
                    html.H5("Running Mean"),
                    html.P("Window"),
                    dcc.Input(id='running-mean-window', type="number", min=3, value=3, step=2),
                    html.P(),
                    html.H5("1-2-1 filter"),
                    html.P("Number of filtering"),
                    dcc.Input(id='121-filter-n', type="number", min=1, max=100, value=1),
                    html.P(),
                    html.H5("Lanczos filter"),
                    html.P("Window"),
                    dcc.Input(id='lanczos-window', type="number", min=3, value=3, step=2),
                    html.P("Cutoff frequency 1"),
                    dcc.Input(id='lanczos-cutoff1', type="number", min=0, max=1, value=0, step=0.0001),
                    html.P(),
                    html.P("Cutoff frequency 2"),
                    dcc.Input(id='lanczos-cutoff2', type="number", min=0, max=1, value=0, step=0.0001),
                    html.P(),
                ]
            )
        else:
            content = html.Div()
            sidebar = html.Div()
    
    elif tab_val == 'uni-tab-spectral':
        if data_uni != None:
            df= pd.read_json(data_uni, convert_dates=True)
            content = html.Div(
                children=[
                    html.Div(
                        dcc.Graph(className='twelve columns',
                            id='spectral-graph',
                            figure=create_spectral_analysis_graph(df)
                        )
                    )
                ]
            )
            sidebar = html.Div(
                children=[
                    html.H3("Settings"),
                    html.Hr(),
                    html.H5("Spectral Analysis"),
                    html.P("Tapper %"),
                    dcc.Input(id='spectral-tapper', type="number", min=1, value=5, step=1),
                    html.P("Window"),
                    dcc.Input(id='spectral-window', type="number", min=3, value=3, step=2),
                    html.P("Significance Level"),
                    dcc.Input(id='spectral-sig', type="number", min=1, max=100, value=5),
                    daq.ToggleSwitch(
                        id='spectral-detrend',
                        label='Detrend',
                        labelPosition='top',
                        value=False
                    )
                ]
            )
        else:
            content = html.Div()
            sidebar = html.Div()
        
    elif tab_val == 'uni-tab-regimeshift':
        if data_uni != None:
            df= pd.read_json(data_uni, convert_dates=True)
            content = html.Div(
                children=[
                    html.Div(
                        dcc.Graph(className='twelve columns',
                            id='regime-graphs',
                            figure=create_regime_shift_graph(df))
                    ),
                ]
            )
            sidebar = html.Div(
                children=[
                    html.H3("Settings"),
                    html.Hr(),
                    html.H5("Regime Shift"),
                    html.P("Cutoff length"),
                    dcc.Input(id='regime-cutoff', type="number", min=1, step=1, value=20),
                    html.P("Significance Level"),
                    dcc.Input(id='regime-sig', type="number", min=1, max=100, value=5),
                ]
            )
        else:
            content = html.Div()
            sidebar = html.Div()


    elif tab_val == 'multi-tab':
        content = html.Div(
            children=[
                dcc.Markdown(infos_ts_multi),
                html.A("Open data example",
                    id='example-multi',
                    download='example_multi.csv',
                    href="https://gitlab.com/enzo.pinheiro/dash-sats/-/raw/master/multi_example.csv",
                    target="_blank"
                ),
                html.Hr(),             
                dcc.Upload(
                    html.Button(
                        "Upload time series",
                        id='upload-button', 
                        style=button_style 
                    ), 
                id='data-upload',
                )
            ],
            style=upload_style
        )
        sidebar = html.Div()
    
    elif tab_val == 'multi-tab-correl':
        if data_multi != None:
            df= pd.read_json(data_multi, convert_dates=True)
            content = html.Div(
                children=[
                    html.Div(
                        dcc.Graph(className='twelve columns',
                            id='correl-heatmap',
                            figure=create_correl_heatmap(df)
                        )
                    )
                ]
            )

            sidebar = html.Div()
        else:
            content = html.Div()
            sidebar = html.Div()

    elif tab_val == 'multi-tab-eof':
        if data_multi != None:
            df = pd.read_json(data_multi, convert_dates=True)
            content = html.Div(
                children=[
                    html.Div(
                        dcc.Graph(className='twelve columns',
                            id='eof-pcs',
                            figure=create_eof_graphs(df)[0]
                        )
                    ),
                    html.Div(
                        dcc.Graph(className='twelve columns',
                            id='eof-table',
                            figure=create_eof_graphs(df)[1]
                        )
                    )
                ]
            )

            sidebar = html.Div()
        else:
            content = html.Div()
            sidebar = html.Div()

    elif tab_val == 'ref-tab':
        content = html.Div(
            children=[
                html.Div(
                    dcc.Markdown(reference_info, style=ref_style)
                )
            ]
        )
        sidebar = html.Div()

    else:
        content = html.Div()
        sidebar = html.Div()
    
    return content, sidebar
