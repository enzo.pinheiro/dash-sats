
import io
import csv
import base64
import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.express as px
import plotly.colors as pc
from plotly.subplots import make_subplots
from pyClimateTools.estatistica import t_test, comp_yule_kendell_index, comp_skewness_coef, regime_shift, filtro_121, filtro_lanczos, analise_espectral, eof
from pyClimateTools.funcoes import rolling_window


def read_data(filename, content):

    if content != "":
        content_type, content_string = content.split(',')
        
        decoded = base64.b64decode(content_string)

        if '.csv' in filename or '.txt' in filename:
            df = pd.read_csv(io.StringIO(decoded.decode('utf-8')), delimiter=' ', parse_dates=True, infer_datetime_format=True)
        elif '.xls' in filename or '.xlsx' in filename:
            df = pd.read_excel(io.BytesIO(decoded), delimiter=' ')
        else:
            df = pd.DataFrame(data=[])
    try:
        col1 = df.columns[0]
        cols = df.columns[1:]
        fig = go.Figure()
        for i in cols:
            fig.add_trace(go.Scatter(x=df[col1], y=df[i]))
        cols_valid = (df.columns.str.contains('Unnamed'))
        if any(cols_valid):
            exit(1)
        if df.shape[1] < 2:
            exit(1)
        outmsg = f'{filename} has been loaded!'
    except:
        df = pd.DataFrame(data=[])
        outmsg = f'{filename} is not a valid data!'

    return df, outmsg


def create_timeseries(df):

    col1 = df.columns[0]
    col2 = df.columns[1]
    ts_mean = np.repeat(df[col2].mean(), df[col2].shape[0])
    ts_std = np.repeat(df[col2].std(), df[col2].shape[0])
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df[col1], y=df[col2], name='timeseries', line=dict(color='black')))
    fig.add_trace(go.Scatter(x=df[col1], y=ts_mean, name='mean', line=dict(color='yellow', dash='dash')))
    fig.add_trace(go.Scatter(x=df[col1], y=ts_mean+ts_std, name='mean+std', line=dict(color='blue', dash='dash')))
    fig.add_trace(go.Scatter(x=df[col1], y=ts_mean-ts_std, name='mean-std', line=dict(color='red', dash='dash')))

    fig.update_layout(
        showlegend=True, 
        font=dict(size=18), 
        hovermode='x', 
        xaxis={'title': col1}, 
        yaxis={'title': col2},
        legend={'orientation': 'h', 'yanchor': 'bottom', 'y':1.02, 'xanchor': 'right', 'x': 1})

    return fig


def create_correlogram(df, nlags=24, stat_sig=5):

    col2 = df.columns[1]
    if nlags != None:
        nlags += 1
    lags = np.arange(0, nlags)
    corrs = []
    sigs = []
    for i in lags:
        if i == 0:
            n = df[col2].shape[0]
            t_crit = t_test(df[col2], stat_sig)/np.sqrt(n)
            corrs.append(np.corrcoef(df[col2], df[col2])[0, 0])
        else:
            n = df[col2].iloc[:-i].shape[0]
            t_crit = t_test(df[col2].iloc[:-i], stat_sig)/np.sqrt(n)
            corrs.append(np.corrcoef(df[col2].iloc[:-i], df[col2].iloc[i:])[0, 1])
        sigs.append(t_crit)
    corr_df = pd.DataFrame(data=np.array([lags, corrs, sigs]).transpose(), columns=['lags', 'correlation', 'significance'])
    
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=corr_df['lags'], y=corr_df['correlation'], name='corr.'))
    fig.add_trace(go.Scatter(x=corr_df['lags'], y=corr_df['significance'], name='up. sig. limit'))
    fig.add_trace(go.Scatter(x=corr_df['lags'], y=-1*corr_df['significance'], name='low. sig. limit'))
    fig.update_layout(
        title_text='Correlogram', 
        xaxis={'title': 'lags'}, 
        yaxis={'title': 'correlation'}, 
        font=dict(size=14), 
        legend={'orientation': 'h', 'yanchor': 'bottom', 'y':1.02, 'xanchor': 'right', 'x': 1})

    return fig


def create_boxplot(df):

    col2 = df.columns[1]

    fig = go.Figure()
    fig.add_trace(go.Box(y=df[col2], marker_color='rgb(7,40,89)', line_color='rgb(7,40,89)', name=col2, boxpoints='outliers'))
    fig.update_layout(title_text='Boxplot', font=dict(size=18))

    return fig


def create_histogram(df):

    col2 = df.columns[1]

    fig = go.Figure()
    fig.add_trace(go.Histogram(x=df[col2], histnorm='probability'))
    fig.update_layout(title_text='Histogram', xaxis={'title': col2}, yaxis={'title': 'probability'}, font=dict(size=18))

    return fig


def create_stat_table(df):

    col2 = df.columns[1]

    q1 = np.percentile(df[col2], 25)
    q2 = np.percentile(df[col2], 50)
    q3 = np.percentile(df[col2], 75)
    iqr = q3 - q1

    ts_max = df[col2].max()
    ts_min = df[col2].min()
    ts_mean = df[col2].mean()
    ts_std = df[col2].std()

    yki = np.round(comp_yule_kendell_index(q1, q2, q3, iqr), 2)
    skew = np.round(comp_skewness_coef(df[col2], ts_mean, ts_std), 2)
    
    stats = ['Interquartile Range', 'Yule Kendell Index', 'Skewness Coef.', 'Minimum', 'Maximum']
    values = [iqr, yki, skew, ts_min, ts_max]

    df_stats = pd.DataFrame(np.array([stats, values]))

    columns = dict(values=['Statistic name', 'Value'])
    cells = dict(values=df_stats.values, height=30)

    fig = go.Figure()
    fig.add_trace(go.Table(
        header=columns,
        cells=cells
        )
    )

    fig.update_layout(title_text='Other statistics', font=dict(size=16))

    return fig


def create_running_mean_graph(df, window=3):
    
    col1 = df.columns[0]
    col2 = df.columns[1]
    
    filtered = np.full(df[col2].shape[0], np.nan)

    filtered[window//2:-(window//2)] = np.nanmean(rolling_window(df[col2], window), 1)

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df[col1], y=df[col2], name='no-filter', line=dict(color='gray')))
    fig.add_trace(go.Scatter(x=df[col1], y=filtered, name=f'running mean', line=dict(color='blue')))
    fig.update_layout(
        title_text='Running Mean', 
        showlegend=True, font=dict(size=18), 
        hovermode='x', 
        xaxis={'title': col1}, 
        yaxis={'title': col2},
        legend={'orientation': 'h', 'yanchor': 'bottom', 'y':1.02, 'xanchor': 'right', 'x': 1})

    return fig


def create_121_filter_graph(df, n=1):
    
    col1 = df.columns[0]
    col2 = df.columns[1]

    filtered = filtro_121(df[col2].values, n)[-1, :]

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df[col1], y=df[col2], name='no filter', line=dict(color='gray')))
    fig.add_trace(go.Scatter(x=df[col1], y=filtered, name=f'1-2-1 filter', line=dict(color='blue')))
    fig.update_layout(
        title_text='1-2-1 Filter', 
        showlegend=True, 
        font=dict(size=18), 
        hovermode='x', 
        xaxis={'title': col1}, 
        yaxis={'title': col2},
        legend={'orientation': 'h', 'yanchor': 'bottom', 'y':1.02, 'xanchor': 'right', 'x': 1})

    return fig


def create_lanczos_filter_graph(df, window=3, cutoff1=0.012, cutoff2=0):
    
    col1 = df.columns[0]
    col2 = df.columns[1]

    filtered = filtro_lanczos(df[col2].values, window, cutoff1, cutoff2)
    
    fig = go.Figure()

    if cutoff1 == 0 and cutoff2 == 0:
        fig.add_trace(go.Scatter(x=df[col1], y=df[col2], name='no-filter', line=dict(color='gray')))
    elif cutoff1 != 0 and cutoff2 == 0:
        fig.add_trace(go.Scatter(x=df[col1], y=df[col2], name='no-filter', line=dict(color='gray')))
        fig.add_trace(go.Scatter(x=df[col1], y=filtered[0], name=f'low-pass filter', line=dict(color='blue')))
        fig.add_trace(go.Scatter(x=df[col1], y=filtered[1], name=f'high-pass filter', line=dict(color='red')))
    else:
        fig.add_trace(go.Scatter(x=df[col1], y=df[col2], name='no-filter', line=dict(color='gray')))
        fig.add_trace(go.Scatter(x=df[col1], y=filtered[2], name=f'band-pass filter', line=dict(color='blue')))
    
    fig.update_layout(
        title_text='Lanczos Filter', 
        showlegend=True, 
        font=dict(size=18), 
        hovermode='x', 
        xaxis={'title': col1}, 
        yaxis={'title': col2},
        legend={'orientation': 'h', 'yanchor': 'bottom', 'y':1.02, 'xanchor': 'right', 'x': 1})

    return fig



def create_spectral_analysis_graph(df, tapper_percent=5, window=3, sig=5, detrend=False):
    
    col1 = df.columns[0]
    col2 = df.columns[1]

    freq, espec, l5, l95 = analise_espectral(df[col2].values, tapper_percent, window, sig, False, detrend)
    
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=freq, y=espec, name='spectrum', line=dict(color='black'), text=[f'Period: {i:.2f} 'for i in 1/freq]))
    fig.add_trace(go.Scatter(x=freq, y=l5, name='low. sig. limit'))
    fig.add_trace(go.Scatter(x=freq, y=l95, name='up. sig. limit'))
    
    fig.update_layout(
        title_text='Spectral Analysis', 
        showlegend=True, 
        font=dict(size=18), 
        hovermode='x', 
        xaxis={'title': 'Frequency'}, 
        yaxis={'title': 'Power'},
        yaxis_type="log",
        legend={'orientation': 'h', 'yanchor': 'bottom', 'y':1.02, 'xanchor': 'right', 'x': 1})

    return fig


def create_regime_shift_graph(df, l=20, sig=1):
    
    col1 = df.columns[0]
    col2 = df.columns[1]

    shifts, rsi = regime_shift(df[col2].values, l, sig)
 
    fig = make_subplots(rows=2, cols=1)
    fig.add_trace(go.Scatter(x=df[col1], y=df[col2], name='timeseries', line=dict(color='black')), row=1, col=1)
    for i in shifts:
        fig.add_shape(
        # Line Vertical
            dict(
                type="line",
                x0=df[col1].iloc[i],
                y0=np.min(df[col2].values),
                x1=df[col1].iloc[i],
                y1=np.max(df[col2].values),
                line=dict(
                    color="red",
                    width=1
                )
            ),
            row=1, col=1
        )
    fig.update_yaxes(title_text=col2, row=1, col=1)
    
    rsis = np.zeros(df[col1].shape)
    rsis[shifts] = rsi
    fig.add_trace(go.Bar(x=df[col1], y=rsis, name='RSI'), row=2, col=1)
    fig.update_xaxes(title_text=col1, row=2, col=1)
    fig.update_yaxes(title_text='RSI', row=2, col=1)


    fig.update_layout(
        title_text='Regime Shift', 
        showlegend=False, 
        font=dict(size=18), 
        hovermode='x', 
        legend={'orientation': 'h', 'yanchor': 'bottom', 'y':1.02, 'xanchor': 'right', 'x': 1},
        height=750
    )

    return fig


def create_correl_heatmap(df, lag=0):

    col1 = df.columns[0]
    cols = [i for i in df.columns[1:]]

    correls = df.corr(method='pearson')

    fig = go.Figure()
    fig.add_trace(go.Heatmap(z=correls, x=cols, y=cols, colorscale=pc.diverging.RdBu_r, zmin=-1., zmax=1.))
    # fig.update_layout(
    #     margin={'l': 100, 'r': 100}
    # )

    return fig


def create_eof_graphs(df, corr=True):

    col1 = df.columns[0]
    cols = [i for i in df.columns[1:]]

    PCs, e, l = eof(df[cols], corr)
    var_perc = pd.DataFrame(data=[[f'PC {m+1}', np.round(j, 2), np.round(j/np.sum(l), 2)] for m, j in enumerate(l)])

    fig1 = go.Figure()
    for n, i in enumerate(PCs):
        fig1.add_trace(go.Scatter(x=df[col1], y=i, name=f'PC {n+1}'))
    fig1.update_layout(
        font=dict(size=18), 
        hovermode='x', 
        xaxis={'title': col1}, 
        legend={'orientation': 'h', 'yanchor': 'bottom', 'y':1.02, 'xanchor': 'right', 'x': 1}
    )

    fig2 = go.Figure()
    columns = dict(values=['PC', 'Variance', '% of total Variance'])
    cells = dict(values=var_perc.values.transpose(), height=30)
    nPCs = var_perc.values.shape[0]
    fig2.add_trace(go.Table(header=columns, cells=cells))
    fig2.update_layout(
        font=dict(size=18)
    )

    return fig1, fig2
