
main_tab_unsel = {
    'height': '57px',
    'borderBottom': '1px solid #d6d6d6',
    'padding': '6px', 
    'background':'#119DFF',
    'font-size': '20px'
}

sec_tab_unsel = {
    'height': '54px',
    'borderBottom': 'solid #d6d6d6',
    'padding': '6px',
    'backgroundColor':'#FFFFFF',
    'color': 'black',
    'margin-top': '3px'
}

main_tab_sel = {
    'height': '57px', 
    'borderTop': '1px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': '#119DFF',
    'color': 'white',
    'padding': '6px',
    'fontWeight': 'bold',
}

sec_tab_sel = {
    'height': '54px', 
    'borderTop': 'solid #d6d6d6',
    'borderBottom': 'solid #d6d6d6',
    'backgroundColor': '#FFFFFF',
    'color': 'black',
    'padding': '6px',
    'fontWeight': 'bold',
    'margin-top': '3px'
}

button_style = {
    "background-color":"#FFFFFF",
    "color":"#007BFF",
}

upload_style = {
    "text-align": "center",
    "margin": "100px"
}

sidebar_style = {
    "top": 0,
    "left": 0,
    "bottom": 0,
    "position": "fixed",
    "min-width": "285px",
    "max-width": "285px",
    "background-color": "#07183a",
    "color": "#ffffff",
    "z-index": "1",
    "overflow": "auto",
    "padding-left": "1rem"
}

main_style = {
    "margin-left":"320px",
}

info_style = {
    'margin-top':50, 
    'margin-left':150, 
    'margin-right':150, 
    'text-align':'center',
    'border':'2px blue solid'}

ref_style = {
    'margin-top':50, 
    'size': 30}

header_style = {
    'backgroundColor': '#07183a', 
    'color': "#ffffff", 
    'text-align': 'center'
}

header_style2 = {
    'backgroundColor': '#07183a', 
    'color': "#ffffff", 
    'text-align': 'center',
    "margin-left":"320px"
}