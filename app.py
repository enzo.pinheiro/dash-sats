
import dash


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server
# cache = Cache(server, config=
#    {
#    'CACHE_TYPE': 'filesystem',
#    'CACHE_DIR': 'cache-directory',
# #    'CACHE_DEFAULT_TIMEOUT': 1800
#    }
# )

app.config.suppress_callback_exceptions = True
