general_infos = '''
### This app performs some univariate and multivariate statistical analyses of time series and 
### provides an interactive way to explore the output graphs.
##### The following univariate statistics are available:
- Exploratory analysis
- Filters
- Spectral analysis
- Regime shift
##### The following multivariate statistics are available:
- Correlation matrix
- Empirical Orthogonal Function
##### Click on Univariate Statistics tab or Multivariate Statistics tab for more information about the input data format.
##### References for the techniques used by this app can be found in the References tab.
'''

infos_ts_uni = '''
#### Upload of time series must be done using the button bellow.
- The file format must be .csv, .txt ou .xmls;
- Separetor must be a single space;
- A header is necessary to identify the time series name and must be in the following format "time values";
'''

infos_ts_multi = '''
#### Upload of time series must be done using the button bellow.
- The file format must be .csv, .txt ou .xmls;
- Separetor must be a single space;
- A header is necessary to identify the time series name and must be in the following format "time values1 values2 ... valuesN";
'''

reference_info = '''
- Duchon, C. E., Lanczos filter in one and two dimensions. J. Applied Meteorology, 18, 1016–1022, 1979.
- Rodionov, S. N., A sequential algorithm for testing climate regime shifts, Geophys. Res. Lett., 31, L09204, 2004. doi:10.1029/2004GL019448.
- Wilks. D. S., Statistical Methods in the Atmospheric Sciences. 3rd edition. New York. Academic Press, 2008. 
'''