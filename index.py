
import dash_html_components as html
import dash_core_components as dcc

import callbacks
from app import app
from styles import main_tab_unsel, main_tab_sel, sec_tab_sel, sec_tab_unsel, button_style, upload_style, info_style, header_style
from infos import general_infos


header = html.Div(
    id='header',
    className='row',
    children=[
        html.Div(className='eleven columns',
            children=[
                html.H3('Statistical Analysis of Time Series Dashboard', 
                    style={
                        'margin-left': 150,
                    }
                ),
            ]
        ),
        html.Div(className='one columns',
            children=[
                html.A([
                    html.Img(
                        src='https://i.imgur.com/2xHw9TF.png',
                        style={
                            'margin-top': 15,
                            'height' : '50%',
                            'width' : '50%',
                            'float': 'bottom',
                        }
                    )
                ],
                href='https://gitlab.com/enzo.pinheiro/dash-sats',
                target="_blank")
            ]
        )
    ],
    style=header_style
)

content = html.Div(
    id='app-content', 
    className='twelve columns',
    children=[
        html.Div(
            id='menu',
            className='row',
            children=[
                dcc.Tabs(
                    id='tabs',
                    children=[
                        dcc.Tab(
                            id='univariate-tab',
                            label='Univariate Statistics',
                            value='uni-tab',
                            style=main_tab_unsel
                        ),
                        dcc.Tab(
                            id='multivariate-tab',
                            label='Multivariate Statistics',
                            value='multi-tab',
                            style=main_tab_unsel
                        ),
                        dcc.Tab(
                            id='reference-tab',
                            label='References',
                            value='ref-tab',
                            style=main_tab_unsel
                        )
                    ]
                ),
                html.Div(
                    id='init-infos',
                    children=[
                        dcc.Markdown(general_infos)
                    ],
                    style=info_style
                )
            ]
        ),

        html.Div(
            id='content',
            className='row',
        ),

        dcc.Store(
            id='df-uni'
        ),

        dcc.Store(
            id='df-multi'
        ),
        html.Div(
            dcc.ConfirmDialog(
                id='output-msg-uni',
            ),
        ),
        html.Div(
            dcc.ConfirmDialog(
                id='output-msg-multi',
            ),
        )
    ], 
)

sidebar = html.Div(
    id='sidebar', 
    className='two columns',
    children=[
        html.H3('Sidebar', style={'text-align':'top'})
    ],
)

main = html.Div(
    id='main',
    className='row',
    children=[sidebar, content]
)

app.layout = html.Div(children=[header, main])


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', dev_tools_ui=True, debug=True)